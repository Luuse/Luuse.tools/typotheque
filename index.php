<?php

require("typotheque/launch.php");

if($_GET){

	$page = $routes->sanitizeString($_GET["page"]);

	if($routes->pageExists($page)) {

		if($page !== "home"){

			if($page == "write-account" && $accounts->isFirst()){

				include("site/pages/".$page.".php");

			}elseif($page == "specimen"){

				if(isset($_GET["var"])){

					$font = $routes->sanitizeString($_GET["var"]);

					if($validate->fontExists($font)){

						include("typotheque/vars/specimen.php");
						include("site/pages/specimen.php");

					}

				}else{

					$routes->redirect($routes->index());

				}


			}else{

				if(!in_array($page, $templates->backPages)){

					include("site/pages/".$page.".php");

				}else{

					if(isset($_SESSION["user"])){

						if(!in_array($page, $templates->adminPages)){

							include("site/pages/".$page.".php");

						}else{

							if(isset($user->admin) && $user->admin == 1){

								include("site/pages/".$page.".php");

							}else{

								$routes->redirect($routes->index());

							}

						}

					}else{

						$routes->redirect($routes->index());
					}
				}


			}

		}else{

			$routes->redirect($routes->index());
		}


	}else{

		header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
		include("site/pages/404.php");


	}




}else{

	if($accounts->isFirst()){

		$_GET["page"] = "new-account";

		include("site/pages/first-account.php");

	}else{

		include("site/pages/home.php");

	}

}

unset($_SESSION["message"]);
?>
