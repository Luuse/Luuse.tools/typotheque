# Typothèque libre
Une typothèque de caractères typographiques sous licence libre. Utilise la librairie javascript [opentype.js](https://github.com/nodebox/opentype.js).


## utilisation
Pour lier au dossier contenant les fontes, il faut changer la variable $path dans inc/variables.php et js/main.js.  
Les dossiers et fichiers ne doivent pas comporter d'espaces.  
Si une info est incorrecte, il est possible d'ajouter un fichiers infos.txt qui écrasera les infos récupérées avec opentype.js (voir infos-sample.txt pour la syntaxe).

## To do
- ~~variantes visibles sur la home en déroulé~~  
- lazy load pour la home ou pages  
- ~~outils de taille, couleur~~
- ~~À propos (c'est quoi ce site + c'est qui qui fait ça + Culture libre et typographie)~~
- nav fontes  
- nav catégories  
- ~~fichier texte qui écrase les infos opentype si incorrectes~~
- ~~spécimen: layout~~  
- ~~spécimen: approches~~  
- spécimen: character map  
- spécimen: points de vecteur  
- spécimen: comparaison  
- css print  
- ~~ajouter conditionnelle si designerURL~~  
- du style globalement  
- ajouter script [rft](https://gitlab.com/atelier-bek/rft)  
- ~~modifications d'un input update tout les inputs~~  
- ~~faire en sorte que ce soit la regular qui apparaisse sur la home~~  
- ~~modification du champ de recherche ne doit pas updater les inputs~~
- ~~Faire un fichier fonte où tout les glyphes sont un symbole pour les glyphes manquants~~
- choisir des textes de spécimen (pourquoi pas corpus de textes typo au choix)  
- modifications d'un input update tout les inputs pour spécimen
- classement home (abc, ajouté, chasse, hauteur d'x...)?  
- améliorer fonte de substitution (soit un glyphe caractère manquant, soit la lettre en question mais suffisamment distincte)
- récupérer nombres de variantes sur la home  
- afficher hauteur d'x + ascendantes + ... sur home en utilisant les infos fontes de x, T, l ,p
- afficher fontsize quand on déplace le slider
- colorpicker afficher code couleur + pouvoir le rentrer manuellement
- ~~outil de recherche home~~
- outil de taille pour spécimen
- virer de nav trier et rechercher dans spécimen
- nav auteurs  
- ~~basculer en .less~~
- outil de recherche pour les auteurs aussi
- hauteur des inputs relative à la hauteur max de la fonte
- bug recherche si pas de résultat tout s'affiche
- ...  


### Bugs
- ~~Certaines url de designers ne sont pas absolues~~  
- ~~Certaines fontes s'affichent en plusieurs versions sur la home~~  

## live version

[ici](http://atelier-bek.be/typotheque)

## captures d'écran

![home](captures/0.png)  
![home](captures/1.png)  
![home](captures/2.png)  
![home](captures/3.png)  
![home](captures/4.png)  
![home](captures/5.png)  
![home](captures/6.png)

## Licence
Ce projet est sous licence [GNU GPL](https://www.gnu.org/licenses/gpl.html).
