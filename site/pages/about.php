<?php include('site/inc/head.php'); ?>
<main>
	<h2>About this Library</h2>
	<form method="POST" action="write-content">
		<div class="infos">
			<?php foreach($form->getForm("about") as $name=>$field): ?>
				<?php $value = $datas->getPageValue("about", $name, $field) ?>
				<?= $form->printLabel($field, $name) ?>
				<?= $form->printInput($field, $name, $value)?>
			<?php endforeach ?>	
			<input type="text" name="from" value="about" readonly style="display: none">
		</div>
		<div class="submit"> 
			<input type="submit" name="envoyer" value="Send">
		</div>
	</form>
</main>
<?php include('site/inc/foot.php') ?>