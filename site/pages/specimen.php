<?php include "site/inc/head.php" ?>
<?php
function preg_it($input, $blue) {
	if (strpos($input, '$$') !== false) {

		$text_preg =  preg_replace_callback("/(\\$\\$\w+)/",

			function($m) use ($blue) {
			
				static $id = -1;
				$id++;
				$r = str_replace('$$', '', $m[$id]);

				if (is_array($blue[$r])) {
					$res = array();	
					foreach($blue[$r] as $f){
						array_push($res, $f);
					}
				} 

				return isset($res) ? $res : $blue[$r];

			}, $input);

		return $text_preg;
		
	} else {

		return $input;

	}

}
?>

<section id="specimen-pages" class="specimen-pages" data-path="<?= $dataPath ?>" style="font-family:<?= $datas->getFontName($fontInfos->fonts->filenames[0]); ?>" >
	<?php foreach ($table["pages"] as $item): ?>
		<?php $class = isset($item["class"]) ? $item["class"] : ""; ?>
		<?php $blocks = isset($item["blocks"]) ? $item["blocks"] : ""; ?>
		<div class="page <?= $class ?>" <?php if(isset($item['page'])){ ?> id="<?= $item['page'] ?>"<?php } ?>>
			<div class="page-inside">
				<?php if(!empty($blocks)): ?>
					<?php foreach ($blocks as $block): ?>
						<div contenteditable="false" class="block <?= preg_it($block['class'], $blue); ?>" <?php if(isset($block['style'])){ ?> style="<?= $block['style'] ?>" <?php } ?>>
							  <?php if (isset($block['text-include'])): ?>
								<?= preg_it(file_get_contents("themes/specimen/".$themeSpecimen."/texts/".$block['text-include'].".txt", "r"), $blue) ?>
							<?php else:?>
								<span class="sock" ><?php echo preg_it($block['text'], $blue); ?></span>
							<?php endif ?>	
						</div>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>
	<?php endforeach ?>
</section>
<ul class="menu" id="menu">
	<?php
		$i = 0;
		foreach($fontInfos->fonts->filenames as $file):
	?>
		
		<button <?php if($i == 0){ echo 'select="true"'; }; ?> class="btn-font" data-file="<?= $file ?>" data-fontname="<?= $datas->getFontName($file) ?>" >
			<?= $datas->getfontname($file) ?>
		</button>

	<?php
		$i++;
		endforeach
	?>
</ul>
<?php include('site/inc/foot.php') ?>
