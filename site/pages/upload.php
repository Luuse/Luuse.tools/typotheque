<?php include('site/inc/head.php'); ?>
<main>
	<h2>Upload Font Family</h2>
	<form method="POST" action="check-font" enctype="multipart/form-data">
		<div class="font-fam">
			<?php foreach($form->getForm("font-upload") as $name=>$field): ?>
				<?= $form->printLabel($field, $name) ?>
				<?= $form->printInput($field, $name)?>
			<?php endforeach ?>	
			<input type="text" name="from" value="upload" readonly style="display: none">
		</div>
		<div class="submit"> 
			<input type="submit" name="envoyer" value="Add">
		</div>
	</form>
</main>
<?php include('site/inc/foot.php') ?>
