<?php include('site/inc/head.php'); ?>
<main>
	<h2>New Account</h2>
	<form method="POST" action="write-account">
		<div class="content">
			<?php foreach($form->getForm("account") as $name=>$field): ?>
				<?= $form->printLabel($field, $name) ?>
				<?= $form->printInput($field, $name)?>
			<?php endforeach ?>	
			<input type="text" name="from" value="new" readonly style="display: none">
		</div>
		<div class="submit"> 
			<input type="submit" name="envoyer" value="Send">
		</div>
	</form>
</main>
<?php include('site/inc/foot.php') ?>
