<?php include('site/inc/head.php'); ?>
<main>	
	<h2>User</h2>
	<form method="POST" action="modify-account">
		<div class="account">
			<?php foreach($form->getForm("account-update") as $name=>$field): ?>
				<?php $value = ($name == "name") ?  $user->$name : '' ?>
				<?= $form->printLabel($field, $name) ?>
				<?= $form->printInput($field, $name, $value)?>
			<?php endforeach ?>	
			<input type="text" name="from" value="new" readonly style="display: none">
		</div>
		<div class="submit"> 
			<input type="submit" name="send" value="Update">
		</div>
	</form>
</main>
<?php include('site/inc/foot.php') ?>
