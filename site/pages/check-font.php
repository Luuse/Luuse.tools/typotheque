<?php

if(isset($_POST["from"])){

	if($_POST["from"] == "upload"){


		if(isset($_FILES["fonts"]["tmp_name"][0]) && strlen($_FILES["fonts"]["tmp_name"][0]) > 0){

			if($validate->isValid($_POST, "font-upload")){


				if($files->writeFontFamily($_POST, $_FILES)){

					include("site/inc/upload/success.php");

				} else {

					include("site/inc/upload/fail.php");

				}
			}
			
		}else{

			$_SESSION["family"] = $_POST["folderName"];
			$_SESSION["errors"] = "Font files can't be sent twice. Thanks to upload them again.";
			$routes->redirect($routes->index()."upload");
		}

		

	}else{

		include("site/inc/upload/success.php");
	}

}else{
	
	$_SESSION["family"] = $_POST["folderName"];
	$_SESSION["errors"] = "Font files can't be sent twice. Thanks to upload them again.";
	$routes->redirect($routes->index()."upload");
}

?>
