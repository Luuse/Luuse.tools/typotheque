<?php include('site/inc/head.php'); ?>
<main>
	<h2>Login</h2>
	<form method="POST" action="check-account">
		<div class="content">
			<?php foreach($form->getForm("login") as $name=>$field): ?>
				<?php $value = $datas->getPageValue("about", $name, $field) ?>
				<?= $form->printLabel($field, $name) ?>
				<?= $form->printInput($field, $name, $value)?>
			<?php endforeach ?>
			<input type="text" name="from" value="new-account" style="display: none" readonly>
		</div>
		<div class="submit">
			<input type="submit" name="send" value="Login">
		</div>
	</form>
</main>
<?php include('site/inc/foot.php') ?>
