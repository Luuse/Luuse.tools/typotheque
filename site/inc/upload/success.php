<?php include("site/inc/head.php") ?>
<?php $values = (empty($_POST)) ? $_SESSION["form"] : $_POST ?>
<?php $folderName = (!isset($values["folderName"])) ? $_SESSION["folderName"] : $values["folderName"]; ?>
<main>
	<section class="message">
		<?php if($values["from"] == "upload"): ?>
			<p>Successfull font upload.</p>
			<p>Please, check the informations below.</br>If some are wrong or missing, you may change them.</p>
		<?php endif ?>
		<?php if(isset($_SESSION["errors"])): ?>
			<?php if(!is_array($_SESSION["errors"])): ?>
				<?= $_SESSION["errors"] ?>
				<?php else: ?>
					<?php foreach ($_SESSION["errors"] as $error): ?>
						<?= $error ?>
					<?php endforeach ?>	
				<?php endif ?>
			<?php endif ?>
		</section>
		<form method="POST" action="write-font" data-path="<?= $strings->getFolderPath($folderName) ?>" id="write-font">
			<h2>Font Family</h2>
			<div class="family">
				<?php foreach($form->getForm("font-family", $themeLibrary) as $name=>$field): ?>
					<?php $value = $datas->setValue($values['from'], $folderName, $name) ?>
					<?php $metas = $datas->setFormMetas($values['from'], $field, $name) ?>
					<?= $form->printLabel($field, $name) ?>
					<?= $form->printInput($field, $name, $value, $metas)?>
					<?php if($field["type"] == "multiple"){ ?>
						<?php $alreadies = (isset($_FILES["fonts"]["name"])) ? ["fonts" => ["filenames" => $_FILES["fonts"]["name"]]] : false ?>
						<?php $count = $form->getCountMultiple($values['from'], $field, $value, $name, $alreadies) ?>
						<div class="subfields" data-form="<?= $name ?>">
							<?php for($i = 0; $i<$count; $i++){ ?>
								<div class="subfield" data-num="<?= $strings->twoFig($i) ?>">
									<?php foreach($field["fields"] as $subname=>$subfield): ?>										
										<?php $subvalue = $datas->setSubvalue($name, $subname, $value, $i, $alreadies) ?>
										<?php $submetas = $datas->setFormMetas($values['from'], $subfield, $name, $subname) ?>
										<?php if($name == "fonts" && $subname == "filenames"): ?>
											<style type="text/css">
											@font-face{ font-family: 'font-<?= $strings->twoFig($i) ?>'; src: url("content/fonts/<?= $strings->clean($folderName) ?>/<?= $subvalue ?>"); }
										</style>
										<div class="charset" style="font-family: 'font-<?= $strings->twoFig($i) ?>';">
										</div>
									<?php endif ?>
									<?= $form->printLabel($subfield, $subname) ?>
									<?= $form->printInput($subfield, $subname, $subvalue, $submetas, $i) ?>
								<?php endforeach ?>
								<?php if(!isset($field["readonly"]) && $i > 0): ?>
									<div class="buttonContent">
										<button class="remove">Remove</button>
									</div>
								<?php endif ?>
							</div>									
						<?php } ?>
					</div>
					<?php if(!isset($field["readonly"])): ?>
						<div class="buttonContent">
							<button id="addButt" data-form="<?= $name ?>">Add Entry</button>
						</div>
					<?php endif ?>
				<?php } ?>
			<?php endforeach ?>	
		</div>
		<input type="text" name="from" value="<?= $routes->current()->uid ?>" readonly style="display: none">
		<div class="submit">
			<input type="submit" name="send" value="Send">
		</div>
	</form>
	<?php if($values["from"] == "home"): ?>
		<form method="POST" action="delete-font" id="delete">
			<input type="text" name="family" value="<?= $folderName ?>" style="display: none;">
			<div class="submit">
				<input type="submit" name="delete" value="Delete Font">
			</div>
		</form>
	<?php endif ?>
</main>
<?php include("site/inc/foot.php") ?>
