	<?php if(isset($_SESSION["errors"])  ||  isset($_SESSION["message"])): ?>
	<section class="messages">
		<?php if(isset($_SESSION["errors"])): ?>
			<div class="errors">
				<?php if(is_array($_SESSION["errors"]) && isset($_SESSION["errors"])): ?>
				<ul>
					<?php foreach($_SESSION["errors"] as $error): ?>
						<li><?= $error ?></li>
					<?php endforeach ?>
				</ul>
				<?php else: ?>	
					<p><?= $_SESSION["errors"] ?></p>
				<?php endif ?>
			</div>
		<?php endif ?>
		<?php if(isset($_SESSION["message"])): ?>
			<div class="message">
				<p>
					<?= $_SESSION["message"] ?>
				</p>
			</div>
		<?php endif ?>
	</section>
	<?php endif ?>