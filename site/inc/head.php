
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>FFL - <?= $about->title ?></title>
	<?= $datas->setStylesheets($themeLibrary, $themeSpecimen) ?>
</head>
<body class="<?= $routes->current()->uid ?>">
	<header>
		<?php if(!$accounts->isFirst()): ?>
			<h1><a href="<?= $routes->index() ?>"><?= $about->title ?></a></h1>
		<?php endif ?>
	<?php if(!$accounts->isFirst()): ?>
		<?php include("site/inc/nav.php");  ?>
	<?php endif ?>
</header>
<?php include("site/inc/errors.php") ?>
