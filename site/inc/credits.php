      <div id="credits">
        <h3>Credits</h3>
        <p>
          <?= $credits->description ?>
        </p>
        <ul class="details">
          <li class="author">
            <span class="title">Author:</span>
            <a href="<?= $credits->author->url ?>">
              <?= $credits->author->name ?>
            </a>
          </li>
          <li>
            <span class="title">Download:</span>
            <a href="<?= $credits->gitUrl ?>">
              <?= $strings->showUrl($credits->gitUrl) ?>
            </a>            
          </li>
          <li>
            <span class="title">License:</span>
            <a href="<?= $credits->license->url ?>">
              <?= $credits->license->name ?>
            </a>              
          </li>
          <li>
            <span class="title">Documentation:</span>
            <a href="<?= $credits->docUrl ?>">
              <?= $strings->showUrl($credits->docUrl) ?>
            </a>             
          </li>
          <li>
            <span class="title">Featuring:</span>
            <?php foreach($credits->tools as $tool){ ?>
              <a href="<?= $tool->url ?>">
                <?= $tool->name ?>
              </a>
            <?php } ?>
          </li>
          <li>
            <span class="title">Languages:</span>
            <?php foreach($credits->languages as $k=>$lang){ ?><?= $lang ?><?php if($k < count($credits->languages)-1){ ?> /<?php } ?> <?php } ?>
          </li>
        </ul>
      </div>