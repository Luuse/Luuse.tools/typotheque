<nav>
  <div class="links">
    <?php if(isset($_SESSION["user"])): ?>
      <div class="fonts">
        <h4>Admin</h4>
        <ul>
          <li><a href="<?= $routes->index() ?>upload">Upload Font</a></li>
          <?php if(isset($user->admin) && $user->admin == 1): ?>
            <li><a href="<?= $routes->index() ?>about">About</a></li>
          <?php endif ?>
        </ul>
      </div>
    <?php endif  ?>
    <div class="accounts">
      <?php if(isset($_SESSION["user"])): ?>
        <h4>User</h4>
        <ul>
					<?php if(isset($user->admin) && $user->admin == 1): ?>
						<li><a href="<?= $routes->index() ?>new-account">New Account</a></li>
					<?php endif ?>
          <li><a href="<?= $routes->index() ?>account">Update</a></li>
          <li><a href="<?= $routes->index() ?>logout">Logout</a></li>
        </ul>
        <?php else: ?>
          <h4><a href="<?= $routes->index() ?>login">Login</a></h4>
        <?php endif ?>
      </div>
    </div>
  </nav>


