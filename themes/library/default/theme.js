
function onWriteTest(){

  var inputs = document.querySelectorAll("input.demo");

  inputs.forEach(function(input){

    input.addEventListener("input", function(){

      var test = input.value;

      inputs.forEach(function(input){

        input.value = test;

      });

    });

  });

}

function hexToComp(hex){

  var rgb = 'rgb(' + (hex = hex.replace('#', '')).match(new RegExp('(.{' + hex.length/3 + '})', 'g')).map(function(l) { return parseInt(hex.length%2 ? l+l : l, 16); }).join(',') + ')';

  rgb = rgb.replace(/[^\d,]/g, '').split(',');

  var r = rgb[0], g = rgb[1], b = rgb[2];

  r /= 255.0;
  g /= 255.0;
  b /= 255.0;
  var max = Math.max(r, g, b);
  var min = Math.min(r, g, b);
  var h, s, l = (max + min) / 2.0;

  if(max == min) {
    h = s = 0;  
  } else {
    var d = max - min;
    s = (l > 0.5 ? d / (2.0 - max - min) : d / (max + min));

    if(max == r && g >= b) {
      h = 1.0472 * (g - b) / d ;
    } else if(max == r && g < b) {
      h = 1.0472 * (g - b) / d + 6.2832;
    } else if(max == g) {
      h = 1.0472 * (b - r) / d + 2.0944;
    } else if(max == b) {
      h = 1.0472 * (r - g) / d + 4.1888;
    }
  }

  h = h / 6.2832 * 360.0 + 0;

  h+= 180;
  if (h > 360) { h -= 360; }
  h /= 360;

  if(s === 0){
    r = g = b = l; 
  } else {
    var hue2rgb = function hue2rgb(p, q, t){
      if(t < 0) t += 1;
      if(t > 1) t -= 1;
      if(t < 1/6) return p + (q - p) * 6 * t;
      if(t < 1/2) return q;
      if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    };

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;

    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }

  r = Math.round(r * 255);
  g = Math.round(g * 255); 
  b = Math.round(b * 255);

  rgb = b | (g << 8) | (r << 16); 
  return "#" + (0x1000000 | rgb).toString(16).substring(1);

}  


function changeBackColor(demos){

  var backColorInput = document.getElementById("backColor");

  backColorInput.addEventListener("input", function(){

    var newColor= this.value;
    var fonts = document.querySelectorAll("section.font");

    fonts.forEach(function(font){

      font.style.backgroundColor=newColor;

    });

    document.body.style.backgroundColor=newColor;


  }); 

}

function changeTextColor(demos){

  var textColorInput = document.getElementById("textColor");

  textColorInput.addEventListener("input", function(){

    var newColor= this.value;
    document.body.style.color=newColor;

    var contrastsElements = document.querySelectorAll('[style*="color:red;"]');

  }); 
}

function changeFontSize(demos){

  var fontSizeInput = document.getElementById("fontSize");

  fontSizeInput.addEventListener("input", function(){

    var newSize = this.value;

    demos.forEach(function(demo){

      demo.style.fontSize=newSize+"px";

    });

  });
}


function tester(){

  var demos = document.querySelectorAll(".demo");

  onWriteTest();
  changeFontSize(demos);
  changeTextColor(demos);
  changeBackColor(demos);

}

function showMoreFont(){

  var shows = document.querySelectorAll(".showMoreFonts");

  shows.forEach(function(show){

    show.addEventListener("click", function(){

      var tester = this.parentElement.querySelector(".moreFonts");
      var name = this.querySelector(".action");
      var sign = this.querySelector(".sign");
      var isOpen = (tester.classList.contains("open"));

      if(isOpen){

        tester.classList.remove("open");
        sign.innerHTML="+";
        name.innerHTML="See All";

      }else{

        tester.classList.add("open");
        sign.innerHTML="-";
        name.innerHTML="Hide All";
      }

    });

  });

}

function showMoreTags(){

  var lists = document.querySelectorAll(".tagList");


  lists.forEach(function(list){

    var title = list.getElementsByTagName("H4")[0];

    list.addEventListener("mouseover", function(){
      var items = list.getElementsByTagName("UL")[0];
      var isOpen = (list.classList.contains("open"));

      if(isOpen){

        list.classList.remove("open");

      }else{

        list.classList.add("open");
      }


    });

    list.addEventListener("mouseout", function(){
      list.classList.remove("open");
    });

  });

}

function listOnTags(tags, name){

  var ons = [];

  tags.forEach(function(subtag){

    if(subtag.classList.contains("on")){

      ons.push(subtag.getAttribute("data-"+name));

    }

  });

  return ons;

}

function noTags(){


  return (onLis.length > 0) ? false : true;

}


function setTags(){

  var lists = document.querySelectorAll(".tagList");

  lists.forEach(function(list){

    var name = list.dataset.name;
    var tags = list.querySelectorAll("li");


    tags.forEach(function(tag){

      tag.addEventListener("click", function(){

        var value = tag.getAttribute("data-"+name);
        var isOn = (tag.classList.contains("on"));
        var fonts = document.querySelectorAll("#fonts .font");

        if(isOn){ tag.classList.remove("on"); }else{ tag.classList.add("on"); }

        var onTags =  listOnTags(tags, name);
        var hasAnyTags = document.querySelector(".tagLists li.on");

        fonts.forEach(function(font){

          var hasThisTag = font.hasAttribute("data-"+name);
          var hasValue = [];

          if(hasAnyTags !== null){

            if(hasThisTag){

              var fontTags = font.getAttribute("data-"+name).split(" / ");

              fontTags.forEach(function(fontTag){

                if(onTags.indexOf(fontTag) == -1){

                  hasValue.push(false);

                }else{

                  hasValue.push(true);
                }

              });

              if(hasValue.indexOf(true) == -1){

                font.classList.add("hidden");

              }else{

                font.classList.remove("hidden");
              }

            }else{

              font.classList.add("hidden");

            }
          }else{

            font.classList.remove("hidden");
          }

        });



      });

    });

  });

}

document.addEventListener("DOMContentLoaded", function(){

    tester();
    showMoreFont();
    showMoreTags();
    setTags();

});
