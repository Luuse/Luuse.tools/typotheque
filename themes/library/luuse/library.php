  <header>
    <div class="flex">
      <?php if($datas->hasFonts()): ?>
        <div class="title">
					<h1><?= $about->title ?></h1>
					<ul>
						<li><a href="#fonts">Fonts</a></li>
						<li><a href="#index">Index</a></li>
						<li><a href="#about">About</a></li>
					</ul>
        </div> 
      <?php endif ?>
      <div class="font-nav">
        <?php if($datas->hasFonts()): ?>
           <!-- <h2><a href="#fonts">Fonts</a></h2> -->
          <div class="tagLists flex">

            <?php if(!empty($datas->metas("types"))): ?>
              <div class="tagList sort" data-name="types">
                <h4>Types</h4>
                <ul>
                  <?php foreach($datas->metas("types") as $num=>$meta): ?>
                    <li data-types="<?= $meta ?>"><?= $meta ?></li>
                  <?php endforeach ?>
                </ul>       
              </div>
            <?php endif ?>
            <?php if(!empty($datas->metas("designer","designerName"))): ?>
              <div class="tagList sort" data-name="designer">
                <h4>Designers</h4>
                <ul>
                  <?php foreach($datas->metas("designer","designerName") as $num=>$meta): ?>
                    <li data-designer="<?= $meta ?>"><?= $meta ?></li>
                  <?php endforeach ?>
                </ul>       
              </div>
            <?php endif ?>
            <?php if(!empty($datas->metas("tags"))): ?>
              <div class="tagList sort" data-name="tag">
                <h4>Tags</h4>
                <ul>
                  <?php foreach($datas->metas("tags") as $num=>$meta): ?>
                    <li data-tag="<?= $meta ?>"><?= $meta ?></li>
                  <?php endforeach ?>
                </ul>  
              </div>
            <?php endif ?>
            <?php if(!empty($datas->metas("license"))): ?>
              <div class="tagList sort" data-name="license">
                <h4>Licenses</h4>
                <ul>
                  <?php foreach($datas->metas("license") as $num=>$meta): ?>
										<li data-license="<?= $meta ?>"><?= $meta ?></li>
									<?php endforeach ?>
								</ul>  
							</div>
            <?php endif ?>
          </div>
				<?php endif ?>
        </div>
    </div>
		<div class="tester">
			<h2>Tester</h2>
			<ul>
				<li><span class="title">Size</span><input id="fontSize" type="range" name="font-size" min="5" max="300" value="100"></li>
				<li><span class="title">Text</span><input id="textColor" type="color" name="text-color" value="#000000"></li>
				<li><span class="title">Back</span><input id="backColor" type="color" name="background-color" value="#ffffff"></li>
			</ul>
		</div>
  </header>
  <main>
    <section id="fonts">
      <?php if($datas->hasFonts()): ?>
        <?php foreach($fonts as $font): ?>
          <section class="font" id="<?= $font->uid ?>"<?php if(isset($font->tags)) { ?> data-tag="<?= $strings->tagsToDataStr($font->tags) ?>" <?php } ?><?php if(isset($font->designer)) { ?> data-designer="<?= $strings->tagsToDataStr($font->designer->designerName) ?>" <?php } ?><?php if(isset($font->license)) { ?> data-license="<?= $font->license ?>" <?php } ?><?php if(isset($font->types)) { ?> data-types="<?= $strings->tagsToDataStr($font->types) ?>" <?php } ?>>
            <header>
              <h3><?= $font->family ?>
              <?php if(isset($_SESSION["user"])): ?>
                <form method="POST" action="modify-font">
                  <input style="display: none;" name="folderName" readonly value="<?= $font->folderName ?>">
                  <input type="text" name="from" value="home" readonly style="display: none">
                  <input type="submit" name="modify" value="✎">
                </form>
              <?php endif ?>
            </h3>
						<ul class="infos" >
							<li class="styles">
								<span class="title"><?= $strings->setMultiple("Style", count($font->fonts->styles)) ?></span>
								<span class="number"><?= count($font->fonts->styles) ?></span>
							</li>
              <li><a href="<?= $font->download ?>" target="_blank"><span class="sign">⬇</span> Download</a></li>
              <li><a href="specimen/<?= $font->uid ?>"><span class="sign">➡</span> Specimen</a></li>
						</ul>
						<ul class="infos">
							<li class="designer">
								<span class="title"><?= $strings->setMultiple("Designer", count($font->designer->designerName)) ?></span>
								<span class="value">
									<?php foreach($font->designer->designerName as $k=>$designer): ?>
										<?php if(!empty($font->designer->designerUrl[$k])): ?>
											<a href="<?= $font->designer->designerUrl[$k] ?>" target="_blank"><?= $designer ?></a>
											<?php else: ?>
                      <?=  $designer ?>
                    <?php endif ?>
                  </br>
                <?php endforeach ?>
              </span>
            </li>
            <?php if(isset($font->types)): ?>
              <li>
                <span class="title"><?= $strings->setMultiple("Type", count($font->types)) ?></span>
                <span class="value"><?= $strings->tagsToStr($font->types) ?></span>
              </li>
            <?php endif ?>
            <?php if(isset($font->license)): ?>
              <li class="license">
                <span class="title">License</span>
                <span class="value">
                  <?php if(isset($font->licenceUrl)): ?>
                    <a href="$font->licenceUrl"><?= $font->licence ?></a>
                    <?php else: ?>
                      <?= $font->license ?>
                    <?php endif ?>  
                  </span>
                </li>
              <?php endif ?>
              <?php if(isset($font->tags)): ?>
                <li class="tags">
                  <span class="title"><?= $strings->setMultiple("Tags", count($font->tags)) ?></span>
                  <span class="value"><?= $strings->tagsToStr($font->tags) ?></span>
                </li>
              <?php endif ?>    
            </ul>
          </header>
            <div class="regular">
             <?php foreach($font->fonts->filenames as $k=>$file):
							 ?>
              <span class="demo <?= $font->template ?>" style="font-family: '<?= $datas->getFontName($file) ?>';"> <?= $datas->setTest($font) ?> </span>
            <?php endforeach ?>
          </div>
        </section>
      <?php endforeach ?>
      <?php else: ?>
        <p>No fonts uploaded yet!</p>
      <?php endif ?>  
    </section>
  </section>
  <?php if($datas->hasFonts()): ?>
    <section id="index">
      <h2>Index</h2>
      <?php foreach($datas->fonts("name") as $num=>$font): ?>
        <div class="font-preview">
          <h3><?= $font->family ?></h3>
          <ul>
            <?php foreach($font->fonts->filenames as $k=>$file): ?>
              <li style="font-family: '<?= $datas->getFontName($file) ?>';">
                <a href="#<?= $font->uid ?>">
                  <?= $font->fonts->styles[$k] ?>
                </a>
              </li>
            <?php endforeach ?> 
          </ul>
        </div>
      <?php endforeach ?>
    </section>
  <?php endif ?>
  <section id="about">
    <h2>About</h2>
    <p>
      <?= $about->about ?>
    </p>
    <?php if(isset($about->author)): ?>
      <div class="author">
        <?php if(isset($about->authorUrl)): ?>
          <a href="<?= $about->aboutUrl ?>">
          <?php endif ?>
          <?= $about->author ?>
          <?php if(isset($about->authorUrl)): ?>
          </a>
        <?php endif ?>
      </div>
    <?php endif ?>
    <?php include('site/inc/credits.php') ?>
  </section>
</main>
