

function showMoreFont(){

  var shows = document.querySelectorAll(".showMoreFonts");

  shows.forEach(function(show){

    show.addEventListener("click", function(){

      var tester = this.parentElement.querySelector(".moreFonts");
      var name = this.querySelector(".action");
      var sign = this.querySelector(".sign");
      var isOpen = (tester.classList.contains("open"));

      if(isOpen){

        tester.classList.remove("open");
        sign.innerHTML="+";
        name.innerHTML="See All";

      }else{

        tester.classList.add("open");
        sign.innerHTML="-";
        name.innerHTML="Hide All";
      }

    });

  });

}

function showMoreTags(){

  var lists = document.querySelectorAll(".tagList");
  

  lists.forEach(function(list){

    var title = list.getElementsByTagName("H4")[0];

    list.addEventListener("mouseover", function(){
      var items = list.getElementsByTagName("UL")[0];
      var isOpen = (list.classList.contains("open"));

      if(isOpen){

        list.classList.remove("open");

      }else{

        list.classList.add("open");
      }


    });

    list.addEventListener("mouseout", function(){

      list.classList.remove("open");

    });

  });

}


function getDataTypes(lists){

  var array = [];

  lists.forEach(function(list){

    array.push(list.dataset.name);

  });

  return array;
}


function hideAll(allFonts){

  allFonts.forEach(function(font){

    font.classList.add("hidden");

  });
}

function showAll(allFonts){

  allFonts.forEach(function(font){

    font.classList.remove("hidden");

  });
}

function onTags(){

  var lists = document.querySelectorAll(".tagList");
  var dataTypes = getDataTypes(lists);
  var onTags = document.querySelectorAll(".tagList li.on");
  var allFonts = document.querySelectorAll(".font");

  if(onTags.length){

    hideAll(allFonts);

    onTags.forEach(function(onTag){

      dataTypes.forEach(function(dataType){

        var attr = onTag.dataset[dataType];

        if(attr){

          var divs = document.querySelectorAll(".font[data-"+dataType+"*='"+attr+"']");

          divs.forEach(function(div){

            if(div.classList.contains("hidden")){

              div.classList.remove("hidden");

            }

          });

        }

      });

    });

  }else{

    showAll(allFonts);

  }
}

function onClickTags(){

  var tags = document.querySelectorAll(".tagList li");

  tags.forEach(function(tag){

    tag.addEventListener("click", function(){

      if(tag.classList.contains("on")){

        tag.classList.remove("on");
        onTags();

      }else{

        tag.classList.add("on");
        onTags();
      }

    });

  });

}

document.addEventListener("DOMContentLoaded", function(){

  showMoreFont();
  showMoreTags();
  onClickTags();
  

});
