<?php

class Files extends Collections{

	public function deleteFont($post){

		$family = $post["family"];
		$dir = $this->fontsDir().$this->clean($family);

		if(file_exists($dir)){

			$files = glob($dir."/*");

			foreach ($files as $file) {
				
				unlink($file);	

			}

			rmdir($dir);

			$_SESSION["message"] = "Font ".$post["family"]." was successfully deleted.";

			$this->redirect($this->index());

		}else{

			$this->redirect($this->index());
		}
	}

	public function writeFontFamily($post, $files){

		if(isset($files['fonts'])) {

			$dir = $this->getFolderPath($post["folderName"]);

			mkdir($dir);
			$uploads = [];


			foreach($files['fonts']['name'] as $key=>$name){

				$filePath = $dir."/".basename($name);
				$uploads [] = (move_uploaded_file($files['fonts']['tmp_name'][$key], $filePath)) ? true : false;

			}

			if(!in_array(false, $uploads)){ 

				return true;

			}else{

				$files = glob($dir.'/*'); 

				foreach($files as $file){ 

					if(is_file($file)){

						unlink($file);
					}
				}

				rmdir($dir);

				return false;

			}
		}

	} 

	public function newSuggestion($type, $suggestion){

		$path = "typotheque/suggestions/".$type.".json";
		$json = $this->list($type);
		$array = [];

		$array [$type] = $json;
		if(!is_array($suggestion)){

			array_push($array[$type], $suggestion);

		}else{

			array_merge($array[$type], $suggestion);
		}

		file_put_contents($path, json_encode($array));

	}

	public function writeForm($post, $destination, $form, $notIn){

		$array = [];

		foreach($post as $field=>$value){

			$value = (!is_array($value) && isset($form[$field]["tags"])) ? $this->strToTags($value) : $value;

			if(!in_array($field, $notIn)){

				$rules = (isset($form[$field])) ? $form[$field] : false;

				if($rules){

					if($rules["type"] == "multiple"){

						$all = [];

						foreach($rules["fields"] as $name=>$subfield){

							if(is_array($post[$name])){

								$names = [];

								foreach($post[$name] as $value){

									$names [] =  trim($value);
								}

								$all [$name] = $names;
							}

						}

						$array[$field] = $all;


					}else{

						if(!is_array($value)){

							if(strlen(trim($value)) > 0){

								$array[$field] = trim($value);

							}

						}else{

							if(count($value) > 0){

								$back = [];

								foreach($value as $item){

									$back [] = trim($item);
								}

								$array[$field] = $back;

							}
						}

					}

				}

			}
		}

		$json =  json_encode($array);
		file_put_contents($destination, $json);
	}

	public function writeFontId($post){

		$dir = $this->fontsDir().$this->clean($post["folderName"])."/";
		$destination = $dir."font-infos.json";
		$form = $this->getForm("font-family");
		$notIn = ["send", "from"];


		if($this->isValid($post, "font-family")){

			$this->writeForm($post, $destination, $form, $notIn);
			$this->printFontStylesheet();
			$this->redirect($this->index());

		}

	}

	public function getFonts($family){

		$files = glob($this->fontsDir().$this->clean($family)."/*");
		$array = [];

		foreach ($files as $file) {

			if($file !== "." && $file !== ".." && $file !== ".DS_Store" && pathinfo($file)["extension"] !== "json"){

				$array [] = substr($file, strrpos($file,"/")+1);

			}
		}

		return $array;
	}

	public function checkForEmptyFonts(){

		$checks = ["home", "upload"];

		if(in_array($this->current()->uid, $checks)){

			$path = $this->fontsDir();
			$dirs = glob($path."*");
			$nots = ["..", ".", ".DS_Store"];
			$infos = [];

			foreach($dirs as $dir){

				if(!in_array($dir, $nots) && !is_file($dir)){

					$hasFontInfos = file_exists($dir."/font-infos.json");

					if(!$hasFontInfos){

						$files = glob($dir."/*");


						foreach ($files as $file) {

							unlink($file);
						}

						rmdir($dir);

					}

				}

			}

		}
	}

	public function writeContent($post){

		$page = $post["from"];
		$destination = "content/".$page."/".$page.".json";
		$form = $this->getForm($page);
		$notIn = ["send", "from"];

		if($this->isValid($post, $page)){

			$this->writeForm($post, $destination, $form, $notIn);
			$this->redirect($this->index());

		}

	}


}

?>