<?php 

class Validate extends Form{

	public function resetSess(){

		unset($_SESSION["errors"]);
		unset($_SESSION["font-family"]);
		unset($_SESSION["message"]);
		unset($_SESSION["family"]);

	}

	public function fontExists($font){

		$dir = $this->fontsDir().$font;

		return(file_exists($dir) && is_dir($dir)) ? true : false;


	}

	public function isNew($post, $blueprint){

		if($blueprint == "font-upload"){

			$dir = $this->fontsDir().$this->clean($post["folderName"]);

			if(file_exists($dir)){

				return false;

			}else{

				return true;
			}

		}else{

			return true;
		}
	}

	public function getErrors($check, $field, $value){

		if($check !== "no_spec_char" && $check !== "maxLength" && $check !== "urls"){

			return false;

		}else{

			if($check == "no_spec_char"){

				if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬]/', $value) && !strpos($value, " ")){

					return "The field '".$field["label"]."' may not contain any special characters."; 

				}else{

					return false;
				}
			}

			if($check == "maxLength"){

				if(count($value) > $field["maxLength"]){

					return $field["label"]." is too long."; 

				}else{

					return false;
				}
			}

			if($check == "urls"){

				$tags = $this->strToTags($value);
				$truths = [];

				if(is_array($tags)){

					foreach ($tags as $tag) {

						if(!filter_var($tag, FILTER_VALIDATE_URL)){

							$truths [] = false;

						}
					}

				}else{

					if(!filter_var($tags, FILTER_VALIDATE_URL)){

						$truths [] = false;
					}
				}

				if(in_array(false, $truths)){

					return "The field '".$field["label"]."' has to contain valid urls.";

				}else{

					return false;
				}

			}
		}
	}

	public function hasErrors($errors){

		return (implode("",array_unique($errors)) == false) ? false : true;
	}


	public function checkForErrors($rules, $checks, $post, $field_name){

		if($rules["type"] !== "file"){

			foreach ($checks as $check) {

				if(isset($rules[$check])){

					if(!isset($rules["array"])){

						$errors [] = $this->getErrors($check, $rules, $post[$field_name]);

					}

				}
			}
		}
	}

	public function isValid($post, $blueprint){

		$this->resetSess();

		$fields = $this->getForm($blueprint);
		$errors = [];

		foreach($fields as $field_name=>$rules){

			$checks = ["maxLength","no_spec_char","urls"];

			if($rules["type"] !== "multiple"){

				$this->checkForErrors($rules, $checks, $post, $field_name);

			}else{

				$subfields = $rules["fields"];

				foreach ($subfields as $subfieldname=>$subrules) {

					$this->checkForErrors($subrules, $checks, $post, $subfieldname);
				}
			}
		}

		$errors = array_values(array_diff($errors, [false]));

		if(!$this->isNew($post, $blueprint)){

			$errors [] = "The font family '".$post["folderName"]."' already exists.";

		}
		
		if($this->hasErrors($errors)){

			$_SESSION["errors"] = $errors;
			$_SESSION["form"] = $post;
			$_SESSION["font-family"] = ($post["folderName"]) ? $post["folderName"] : "";

			$this->redirect($this->index().$post["from"]);	

		}else{

			return true;

		}
	}




}


?>