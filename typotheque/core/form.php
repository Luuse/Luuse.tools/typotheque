<?php

class Form extends Strings{

	public function getLastFilename($path){

		$start = strrpos($path, "/")+1;
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		$length = strlen($path) - ($start + strlen($ext));

		return substr($path, $start, $length);

	}

	public function getBlueprintPath($blueprint){

		return "typotheque/blueprints/".$blueprint.".json";

	}

	public function getForm($blueprint, $themeLibrary = false){

		$path = ($themeLibrary == false) ? $this->getBlueprintPath($blueprint) : "themes/library/".$themeLibrary."/".$blueprint.".json";

		return json_decode(file_get_contents($path), true);


	}

	public function setName($field, $name){

		return (isset($field["array"])) ? 'name="'.$name.'[]"' : 'name="'.$name.'"';

	}

	public function hasSuggestions($name, $type){

		return ($name !== "modified" && $name !== "created" && $type !== "multiple") ? true : false;
	}

	public function getSubfields($fields){

		$array = [];

		foreach($fields as $name=>$rules){

			$array [] = $name; 
		}

		return implode(",", $array);
	}

	public function setId($name, $key){

		return ($key !== false) ? $name.'-'.$this->twoFig($key) : $name;
	}

	public function printLabel($field, $name, $key = false){

		$required = (isset($field["required"])) ? 'required' : '';
		$tags = (isset($field["tags"])) ? 'tags' : '';
		$multiple = ($field["type"] == "multiple") ? "data-multiple='true'" : '';
		$children = ($field["type"] == "multiple") ? 'data-children="'.$this->getSubfields($field["fields"]).'"' : '';
		$name = $this->setId($name, $key);

		if($field["type"] !== "radio" && $field["type"] !== "checkboxes"){

			if($field["type"] == "title"){

				return '<h3>'.$field["label"].'</h3>';

			}else{

				return (!isset($field["invisible"])) ? '<label for="'.$name.'" class="'.$required.' '.$tags.'" '.$children.' '.$multiple.'>'.$field["label"].'</label>' : '';

			}

		}

	}

	public function getSuggestions($name, $metas){

		if($metas !== false && isset($metas)){

			$array = [];

			foreach($metas as $suggestion){

				$array [] = '<option value="'.$suggestion.'">';
			}

			return implode("", $array);

		}

	}

	public function getAllThemes($type, $value){

		$files = glob("themes/".$type."/*");
		$back = [];

		foreach ($files as $file){
			
			if(is_dir($file)){

				$array = [];

				$name = $this->getLastFilename($file);

				$array["name"] = $name;
				$array["label"] = $this->firstCap($name);

				if($name == $value){

					$array["checked"] = 1;

				}

				$back [] = $array;
			}
		}

		return $back;
	}

	public function printInput($field, $name, $value = false, $metas = false, $key = false){

		if($field["type"] !== "title"){

			$value2 = ($field["type"] !== "multiple" && $field["type"] !== "checkboxes") ? 'value="'.$value.'"' : '';
			$standardInputs = ["text","number","url", "multiple", "password", "file"];

			if(in_array($field["type"], $standardInputs)){

				$suggestionLink = ($this->hasSuggestions($name, $field["type"])) ? 'list="'.$name.'-list"' : '';
				$suggestions = ($this->hasSuggestions($name, $field["type"])) ? '<datalist id="'.$name.'-list">'.$this->getSuggestions($name, $metas).'</datalist></br>' : '';
				$multiple = ($field["type"] == "multiple") ? 'class="invisible"' : '';

				return '<input id="'.$this->setId($name, $key).'" '.$suggestionLink." ".$this->printRules($field, $name).' '.$value2.' '.$multiple.'>'.$suggestions;

			}elseif($field["type"] == "textarea"){

				return '<textarea '.$this->printRules($field, $name).'>'.$value.'</textarea>';

			}elseif($field["type"] == "radio" || $field["type"] == "checkboxes"){

				$required = (isset($field["required"])) ? 'required' : '';
				$inputs = ['<label class="title '.$required.'">'.$field["label"].'</label>'];

				if(isset($field["theme"])){

					$field["values"] = $this->getAllThemes($field["theme"], $value);

				}

				foreach ($field["values"] as $k => $subvalue) {
					
					$arr =  (isset($subvalue["array"])) ? "[]" : "";
					$id = ($field["type"] == "checkboxes") ? $subvalue["value"]  : $subvalue["name"];
					$label = '<label for="'.$id.'">'.$subvalue["label"].'</label>';
					$type = ($field["type"] == "checkboxes") ? "checkbox" : $field["type"];
					$name = ($field["type"] == "checkboxes") ? $subvalue["name"].$arr : $name;

					if(isset($value)){

						if($field["type"] == "checkboxes"){

							if(is_array($value)){

								if(in_array($subvalue["value"], $value)){

									$checked = "checked";

								}else{

									$checked = '';
								}

							}else{

								if($subvalue["value"] !== $value){

									$checked = (isset($subvalue["checked"])) ? "checked" : '';

								}
							}

						}else{

							if($subvalue["name"] == $value){

								$checked = "checked";

							}else{

								$checked = (isset($subvalue["checked"])) ? "checked" : '';

							}						
						}

					}

					$input = '<input type="'.$type.'" id="'.$id.'" name="'.$name.'" value="'.$id.'" '.$checked.'>';
					$inputs [] =  $input." ".$label;

				}

				return '<div class="radio">'.implode(" ", $inputs)."</div>";
			}

		}
	}

	public function printRules($field, $name){

		$rules = [];
		$nots = ["no_spec_char", "label", "array", "invisible", "tags", "fields"];
		$name = $this->setName($field, $name);
		$invisible = (isset($field["invisible"])) ? 'class="invisible"' : "";
		$tags = (isset($field["tags"])) ? 'data-field-type="tags"' : "";

		foreach($field as $rule=>$value){

			if(!in_array($rule, $nots)){

				$value = ($value == 1) ? "true" : $value;
				
				if($field["type"] !== "textarea"){

					$rules [] = $rule.'="'.$value.'"';

				}

			}	

		}

		array_push($rules, $name, $invisible, $tags);

		return implode(" ", $rules);

	}

	public function printNames($field, $file){

		$alredys = ["filenames", "name"];

		if($field == "filenames"){
			
			$value = $file;

		}elseif($field == "name"){

			$value = $this->mightFontname($file);

		}

		return (in_array($field, $alredys)) ? 'value="'.$value.'"' : "";
	}

	public function printFam($field, $fam, $from){

		$alredys = ["family", "created", "modified"];

		if($field == "family"){
			
			$value = $fam;

		}elseif($field == "created" || $field == "modified"){

			if($from !== "home"){

				$now = new Datetime();
				$value = $now->format('U');

			}else{

				if($field == "modified"){

					$now = new Datetime();
					$value = $now->format('U');

				}

			}

		}

		if($value){

			return (in_array($field, $alredys)) ? 'value="'.$value.'"' : "";

		}
	}


	public function firstIndex($a) {

		foreach ($a as $k => $v) return $k; 

	}
	public function getFirstLabel($fields){

		foreach($fields as $name=>$content){

			return $name;
		}
	}

	public function getCountMultiple($from, $field, $value, $name, $alreadies = false){

		$first = $this->getFirstLabel($field["fields"]);

		if($from == "upload"){

			if(isset($alreadies[$name])){

				$subname = $this->firstIndex($alreadies[$name]);

				return (isset($alreadies[$name][$subname])) ? count($alreadies[$name][$subname]) : 1;

			}else{

				return 1;
			}

		}else{

			return count($value->$first);

		}

	}
}

?>