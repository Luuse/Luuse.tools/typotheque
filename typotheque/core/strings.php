<?php

class Strings extends Routes{

	// GENERIC

	public function setMultiple($str, $var, $exception = false){

		if(!$exception){

			return ($var > 1) ? $str."s" : $str;

		}else{

			return ($var > 1) ? $exception : $str;
		}
	}

	public function clean($str) {

		$string = str_replace(' ', '-', $str); 
		
		return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string)); 
	}

	public function cleanKeepCase($str) {

		$string = str_replace(' ', '-', $str); 
		
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); 
	}

	public function tagsToStrForm($tags){

		return "[ ".implode(" ][ ", $tags)." ]";

	}

	public function tagsToDataStr($tags){

		return (is_array($tags)) ? implode(" / ", $tags) : $tags;
	}

	public function tagsToStr($tags){

		return (is_array($tags)) ? implode("</br>", $tags) : $tags;
	}

	public function hasTags($str, $one, $two){


		return (strpos($str, $one) !== false && strrpos($str, $two) !== false) ? true : false;
	}

	public function toBeSplited($str){

		$one = "[";
		$two = "]";

		if($this->hasTags($str, $one, $two)){

			return trim(substr($str, strpos($str, $one)+1, strrpos($str, $two)-1));

		}else{

			return trim($str);
		}

	}

	public function strToTags($str){

		if(!empty($str)){

			$delim = "][";
			$str = $this->toBeSplited($str);

			if(strpos($str, $delim) !== false){

				$tags = explode($delim, $str);
				$back = [];

				foreach($tags as $tag){

					$back [] = trim($tag);
				}

				return $back;

			}else{

				return [$str];
			}

		}else{

			return [];
		}

	}

	public function showUrl($url){

		$begins = ["http://www.", "https://www.", "http://", "https://"];
		$back = [];

		foreach($begins as $begin){

			$sample = substr($url, 0, strlen($begin));

			if($sample == $begin){

				if(substr($url, strlen($url)-1, strlen($url)-2) == "/"){

					$back []  = substr($url, strlen($begin), strlen($url)-strlen($begin)-1);

				}else{

					$back []  = substr($url, strlen($begin));
				}


			}
		}

		if(count($back) > 0){

			return $back[0];

		}


	}

	public function firstCap($str){

		return strtoupper(substr($str, 0, 1)).substr($str, 1);
	}

	public function twoFig($fig){

		$fig = $fig+1;

		if(strlen($fig) == 1){

			return "0".strval($fig);

		}else{

			return strval($fig);
		}
	}

	public function num($int){

		$fig = strval($int+1);

		return ($int < 9) ? "0".$fig : $fig;
	}

	public function multiName($name, $int){

		return $name."-".$this->num($int);
	}

	public function getFolderPath($family){

		return $this->fontsDir().$this->clean(strtolower($family));
	}

	public function mightFontname($filename){

		return preg_replace('/[^A-Za-z0-9\-]/', ' ',substr($filename, 0, strpos($filename, "."))); 

	}

}

?>