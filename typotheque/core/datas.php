<?php

class Datas extends Strings{ 

	public function getForm($blueprint){
		
		return json_decode(file_get_contents("typotheque/blueprints/".$blueprint.".json"), true);
	}

	public function setName($field, $name){

		return ($field["array"]) ? 'name="'.$name.'[]"' : 'name="'.$name.'"';

	}

	public function printRules($field, $name){

		$rules = [];
		$nots = ["no_spec_char", "label", "array", "invisible"];
		$name = $this->setName($field, $name);
		$invisible = ($field["invisible"]) ? 'class="invisible"' : "";

		foreach($field as $rule=>$value){

			if(!in_array($rule, $nots)){

				$value = ($value == 1) ? "true" : $value;
				$rules [] = $rule.'="'.$value.'"';		

			}	

		}

		return $name." ".$invisible." ".implode(" ", $rules);

	}

	public function printNames($field, $file){

		$alredys = ["filename", "name"];

		if($field == "filename"){
			
			$value = $file;

		}elseif($field == "name"){

			$value = $this->mightFontname($file);

		}

		return (in_array($field, $alredys)) ? 'value="'.$value.'"' : "";
	}

	public function printFam($field, $fam, $from){

		$alredys = ["family", "created", "modified"];

		if($field == "family"){
			
			$value = $fam;

		}elseif($field == "created" || $field == "modified"){

			if($from !== "home"){

				$now = new Datetime();
				$value = $now->format('U');

			}else{

				if($field == "modified"){

					$now = new Datetime();
					$value = $now->format('U');

				}

			}

		}

		if($value){

		return (in_array($field, $alredys)) ? 'value="'.$value.'"' : "";

		}
	}
}


?>