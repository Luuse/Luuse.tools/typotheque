<?php

class Collections extends Validate{

	public function getFontInfos($folderName){

		$infos = json_decode(file_get_contents("content/fonts/".$this->clean($folderName)."/font-infos.json"));

		if($infos){

			return $infos;

		}else{

			$this->redirect($this->index());
		}

	}

	public function created($a, $b){

		return strcmp($a->created, $b->created);
	}

	public function fonts($order){

		$path = $this->fontsDir();
		$dirs = glob($path."*");
		$nots = ["..", ".", ".DS_Store"];
		$infos = [];

		foreach($dirs as $dir){

			if(!in_array($dir, $nots)){

				if(file_exists($dir."/font-infos.json")){

					$content = json_decode(file_get_contents($dir."/font-infos.json"));

					$info = $content;
					$info->uid = substr($dir, strrpos($dir, "/")+1);
					$infos [] = $info;

				}

			}

		}

		if($order == "name"){

			return $infos;

		}elseif($order == "last"){

			usort($infos, array($this, "created"));

			return array_reverse($infos);

		}
	}


	public function theListMetas(){

		$metas = json_decode(file_get_contents("typotheque/blueprints/font-family.json"));
		$theList = [];

		foreach ($metas as $name => $meta) {

			$theList [] = $name;
		}

		return $theList;
	}



	public function theList($name, $subname = false){


		$suggest = ($subname == false) ? $name : $subname;
		$file = "typotheque/suggestions/".$suggest.".json";

		if(file_exists($file)){

			$json = json_decode(file_get_contents($file));

			return $json->$suggest;

		}else{

			return false;
		}

	}

	public function metas($meta, $submeta = false){

		$fonts = $this->fonts("name");
		$metas = [];

		foreach ($fonts as $font){

			if(isset($font->$meta)){

				$value = ($submeta == false) ? $font->$meta : $font->$meta->$submeta;

				if(isset($value)){

					if(!is_array($value)){

						$metas [] = $value;

					}else{

						$metas = array_merge($metas, $value);
					}

				}

			}

			if($this->theList($meta, $submeta)){

				$theList = $this->theList($meta, $submeta);

				$metas = array_merge($metas, $theList);

			}

		}

		$array = array_filter(array_unique($metas));
		natcasesort($array);

		return $array;


	}


	public function getRegular($family){

		$weights = ["Book", "Medium", "Regular"];

		foreach ($weights as $weight){

			if(in_array($weight, $family->styles)){

				$key = array_search($weight, $family->styles);

				$regular = $family->filename[$key];
			}
		}
		return (!isset($regular)) ? $family->filename[0] : $regular;
	}

	public function getFontName($file){

		return $this->clean(substr($file, 0, strrpos($file, ".")));
	}

	public function printFontStylesheet(){

		$fonts = $this->fonts("name");
		$styles = [];

		foreach($fonts as $font){

			foreach($font->fonts->filenames as $key=>$file){

				$styles [] = '@font-face{ font-family: "'.$this->getFontName($file).'"; src:url("../../content/fonts/'.$font->uid.'/'.$file.'"); }';

			}
		}

		$stylesheet = implode("\n", $styles);

		file_put_contents("assets/css/fonts.css", $stylesheet);


	}

	public function setStylesheets($themeLibrary, $themeSpecimen = false){

		$finalCall = [];
		$stylesheets = [];

		$stylesheets [] = $this->index()."assets/css/fonts.css";


		if($this->current()->uid !== "home" && $this->current()->uid !== "specimen"  || $themeSpecimen == "default" && $this->current()->uid == "specimen"  || $themeLibrary == "default" && $this->current()->uid == "home"){

			$stylesheets [] = $this->index()."assets/css/style.css";

		}

		if($this->current()->uid == "home"){

			$stylesheets [] = $this->index()."themes/library/".$themeLibrary."/css/style.css";

		}


		if(isset($themeSpecimen) && $this->current()->uid == "specimen"){

			$stylesheets [] = $this->index()."themes/library/".$themeLibrary."/css/style.css";
			$stylesheets [] = $this->index()."themes/specimen/".$themeSpecimen."/style.css";

		}

		foreach ($stylesheets as $stylesheet) {

			$finalCall [] = '<link rel="stylesheet" type="text/css" href="'.$stylesheet.'" />';

		}

		return implode("\n", $finalCall);

	}


	public function setTest($font){

		return (isset($font->test_words) && strlen($font->test_words) > 0) ? $font->test_words : "Hello world";

	}

	public function nowDate(){

		$now = new Datetime();
		return $now->format('U');
	}

	public function setValue($from, $folderName, $name, $prevValue = false){

		$form = $this->getForm("font-family");

		$value = ($name == "modified") ? $this->nowDate() : "";

		if($prevValue){

			$value = $prevValue;

		}else{

			if($from == "home"){

				$font = $this->getFontInfos($folderName);

				if(isset($font->$name)){

					$value = (!isset($form[$name]["tags"])) ? $font->$name : $this->tagsToStrForm($font->$name);

				}else{

					$value = "";
				}


			}elseif($from == "upload"){

				if($name == "filenames"){

					$value = $filename;

				}

				if($name == "created"){

					$value = $this->nowDate();
				}


				if($name == "family"){

					$value = $folderName;

				}

				if($name == "folderName"){

					$value = $this->clean($folderName);

				}
			}
		}

		return (!empty($value)) ? $value : "";
	}

	public function setSubvalue($name, $subname, $value, $i, $alreadies = false){

		if(isset($value->$subname[$i])){

			return $value->$subname[$i];

		}else{

			if(isset($alreadies[$name][$subname])){

				return $alreadies[$name][$subname][$i];

			}else{

				return false;

			}
		}
	}

	public function setFormMetas($from, $field, $name, $subname = false){

		$noMetas = ["title", "radio", "checkboxes", "multiple"];

		if(!in_array($field["type"], $noMetas)){

			return $this->metas($name, $subname);

		}else{

			return false;
		}
	}

	public function getRegNeedle($name, $styles){

		return (in_array($name, $styles)) ? array_search($name, $styles) : false;
	}

	public function returnReg($font, $key){

		$this->style = $font->fonts->styles[$key];
		$this->filename = $font->fonts->filenames[$key];

		return $this;
	}

	public function getRegularFont($font){

		$regularKey = $this->getRegNeedle("Regular", $font->fonts->styles);
		$mediumKey = $this->getRegNeedle("Medium", $font->fonts->styles);
		$bookKey = $this->getRegNeedle("Book", $font->fonts->styles);

		if($regularKey ||  $mediumKey ||  $bookKey){

			if($regularKey){

				return $this->returnReg($font, $regularKey);

			}elseif($mediumKey){

				return $this->returnReg($font, $mediumKey);

			}else{

				return $this->returnReg($font, $bookKey);
			}

		}else{

			return $this->returnReg($font, 0);
		}
	}

	public function removeRegular($font, $key){

		unset($font->fonts->styles[$key]);
		unset($font->fonts->filenames[$key]);

		$font->fonts->styles = array_values($font->fonts->styles);
		$font->fonts->filenames = array_values($font->fonts->filenames);

		return $font->fonts->filenames;

	}

	public function getUnregular($font){

		$regularKey = $this->getRegNeedle("Regular", $font->fonts->styles);
		$mediumKey = $this->getRegNeedle("Medium", $font->fonts->styles);

		if($regularKey ||  $mediumKey){

			if($regularKey){

				return $this->removeRegular($font, $regularKey);

			}else{


				return $this->removeRegular($font, $mediumKey);
			}

		}else{

			return $this->removeRegular($font, 0);
		}
	}

	public function hasFonts(){

		$fonts = $this->fonts("name");

		return (!empty($fonts)) ? true : false;
	}

	public function getContent($page){

		$file = "content/".$page."/".$page.".json";

		if(file_exists($file)){

			return json_decode(file_get_contents($file));

		}else{

			return false;
		}
	}

	public function getCredits(){

		$file = "typotheque/credits/credits.json";

		return json_decode(file_get_contents($file));

	}

	public function getPageValue($page, $name, $field){

		$file = "content/".$page."/".$page.".json";
		$datas =  json_decode(file_get_contents($file));

		if($field["type"] !== "title" && isset($datas->$name)){

			return (!isset($field["tags"])) ? $datas->$name : $this->tagsToStrForm($datas->$name);

		}else{

			return false;
		}

	}
}


?>
