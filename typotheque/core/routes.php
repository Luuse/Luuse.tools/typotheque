<?php

class Routes{

	public function sanitizeString($str){

		return filter_var($str, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	}

	public function index(){

		$protocol = (isset($_SERVER["HTTPS"])) ? "https://" : "http://";

		return $protocol.$_SERVER['HTTP_HOST'].substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], "index.php"));
	}

	public function pageExists($page){

		$file = "site/pages/".$page.".php";

		return (file_exists($file)) ? true : false;
	}

	public function redirect($url){

		header('Location: '.$url);
		exit();
	}

	public function current(){

		$url = $_SERVER['REQUEST_URI'];
		$page = (isset($_GET["page"])) ? $this->sanitizeString($_GET["page"]) : false;

		if(!$page){

			$this->uid = "home";
		
		}else{

			$this->uid = $page;

		}

		$this->url = $url;

		return $this;
	}

	public function fontsDir(){

		return "content/fonts/";
	}

}

?>