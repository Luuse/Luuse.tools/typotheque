<?php

class Accounts extends Validate{

	public function getAccount($name){

		$path = "accounts/".$this->clean($name).".json";
		$infos = json_decode(file_get_contents($path));

		return $infos;

	}

	public function isFirst(){

		$accounts = glob("accounts/*.json");

		return (empty($accounts)) ? true : false;
	}


	public function modifyAccount($post){

		$path = "accounts/".$this->clean($post["name"]).".json";
		$old = $this->getAccount($post["name"]);

		if(password_verify($post["oldPass"], $old->pass)){

			$data = json_encode(["name"=>$old->name, "pass"=>password_hash($post["newPass"], PASSWORD_BCRYPT), "admin"=>$old->admin]);
			file_put_contents($path, $data);
			$_SESSION["message"] = "Password changed!";
			$this->redirect($this->index());


		}else{

			$_SESSION["errors"] = "Wrong old password.";
			$this->redirect($this->index()."account");

		}


	}

	public function writeAccount($post){

		$path = "accounts/".$this->clean($post["name"]).".json";

		if($this->isValid($post, "account")){

			$data = json_encode(["name"=>$post["name"], "pass"=>password_hash($post["pass"], PASSWORD_BCRYPT), "admin"=>intval($post["admin"])]);

			if(!file_exists($path)){

				file_put_contents($path, $data);
				$_SESSION["message"] = "New user successfully added !";
				$this->redirect($this->index());

			}else{

				$_SESSION["errors"] = "User '".$post["name"]."' already exists.";
				$this->redirect($this->index()."new-account");

			}

		}


	}

	public function checkAccount($post){

		$infos = $this->getAccount($post["name"]);

		if($this->isValid($post, "login")){

			if(!$infos){

				$_SESSION["errors"] = "User '".$post["name"]."' can't be found.";
				$this->redirect($this->index()."login");

			}else{

				if(password_verify($post["pass"], $infos->pass)){

					$_SESSION["user"] = $infos->name;
					$_SESSION["message"] = "Welcome ".$post["name"]." :)";

					$this->redirect($this->index());

				}else{

					$_SESSION["errors"] = "Wrong password.";
					$this->redirect($this->index()."login");
				}

			}

		}

	}
}

?>
