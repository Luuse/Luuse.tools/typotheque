<?php

class Templates extends Strings{

	public $frontPages = ["specimen","login","home"];
	public $backPages =  ["check-font","write-font","modify-font","delete-font","account","new-account","write-account","logout", "upload", "infos", "about", "write-content"];
	public $adminPages = ["about", "new-account"];
	public $errorPages = ["404"];

	public function isFrontPage(){

		$uid = $this->current()->uid;

		return (in_array($uid, $this->frontPages)) ? true : false;
	}

	public function isAdminPage(){

		$uid = $this->current()->uid;

		return (in_array($uid, $this->adminPages)) ? true : false;

	}

	public function isHomePage(){

		$uid = $this->current()->uid;

		return ($uid == "home") ? true : false;
	}

	public function setScriptLink($path, $name){

		$root = $this->index();
		$file = $path."/".$name.'.js';
		return (file_exists($file)) ? '<script type="text/javascript" src="'.$root.$file.'"></script>' : "";
	}


	public function writeScriptLinks($scripts, $theme = false){

		$path = "assets/js";
		$loads = [];

		foreach($scripts as $script) {

			$loads [] = $this->setScriptLink($path, $script);
		}

		if($theme){

			$loads [] = $this->setScriptLink("themes/library/".$theme."/js", "theme");
		}


		return implode("\n", $loads);
	}

	public function setScripts($theme = false){

		$uid = $this->current()->uid;

		if($uid == "specimen"){

			$scripts = ["lib/opentype.min", "system/specimen"];

			

		}else{

			$scripts = ["lib/opentype.min","system/main"];

		}

		return ($scripts) ? $this->writeScriptLinks($scripts, $theme) : false;

	}

	public function isValidLibTheme($theme){

		$dir = "themes/library/".$theme;
		
		return (file_exists($dir) && is_dir($dir)) ? true : false;
	}

	public function loadLibTheme($themeLibrary){

		if($this->isValidLibTheme($themeLibrary)){

			return "themes/library/".$themeLibrary."/library.php";

		}else{

			return "themes/library/default/library.php";
		}
	}
}
