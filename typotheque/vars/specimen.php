<?php

/* SPECIMEN VARS */

$fontInfos = $datas->getFontInfos($_GET['var']);
$dataPath = $fontInfos->folderName;
$themeSpecimen = $fontInfos->themeSpecimen;
$table = Spyc::YAMLLoad('themes/specimen/'.$themeSpecimen.'/blueprints.yaml');

// blueprints variables
$blue = array();
$blue['family'] = $fontInfos->family;
$blue['test_words'] = $fontInfos->test_words;
$blue['template'] = $fontInfos->template;
$blue['designerName'] = $fontInfos->designer->designerName;

?>
