<?php 

/* CLASS */

include("core/routes.php");
include("core/strings.php");
include("core/templates.php");
include("core/form.php");
include("core/validate.php");
include("core/accounts.php");
include("core/collections.php");
include("core/files.php");

session_start();

/* VARIABLES / LIBRAIRIES */

include("vars/class.php");
include("lib/load.php");
include("vars/globals.php");


$files->checkForEmptyFonts();


