<?php
ini_set('display_errors', 'On'); # mode debug, off sur serveur !!!!

function mergeJson(){
  $paths = [];
  $i = 0;
  $dir = "content/fonts";
  $fileinfos = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($dir)
  );
  foreach($fileinfos as $pathname => $fileinfo) {
      $filename = $fileinfo->getFilename();
      if (!$fileinfo->isFile()) continue;
      $ext = substr($fileinfo, -5);
      if ($ext == '.json' && $filename != 'globalJson.json') {
        $paths[$i] = $fileinfo;
        $i++;
      }
  }
  $i = 0;
  $globalJson = [];
  foreach ($paths as $path) {
    // echo $path;
    $content = file_get_contents($path);
    $content = json_decode($content);
    $i++;
    print_r($content);
    $globalJson[$i] = $content;
  }

  $fileName = $dir.'/globalJson.json';
  $globalJson = json_encode($globalJson);

  $file = fopen($fileName, 'w');
  fwrite($file, $globalJson);
  fclose($file);
  echo 'File <em>' . $fileName . '</em> written in directory. <br>';

}

mergeJson();
