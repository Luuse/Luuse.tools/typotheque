/* ---- CHECK-FONT / MODIFY-FONT ---- */

function enterPressed(e) {

  e = e || window.event;
  var charCode = e.keyCode || e.which;

  return (charCode == 13) ? true : false;

};

function numi(int){

  var fig = int+1;

  return (int < 9) ? "0"+fig : fig;
}

function setValues(metas, general, i){

  for(name in metas){

    var id =  (general) ? name : name+"-"+numi(i);
    var value = metas[name];

    if(value && document.getElementById(id)){

      document.getElementById(id).value=value;

    }
  }
}

function separateUpper(str){

  var splited = str.split("");

  splited.forEach(function(letter, i){

    if(letter == letter.toUpperCase()){

      splited[i] = " "+letter;
    }

  });

  return splited.join('');

}


function styleName(fontName){

  var space = fontName.indexOf(" ");

  if(space == -1){

    return "";

  }else{

    return fontName.slice(space+1, fontName.length);
  }

}

function setStyles(fontName){


  var fontName = separateUpper(fontName);
  var mightNeedles = ["-", "_"]; 

  if(fontName.indexOf(mightNeedles[0]) !== -1 || fontName.indexOf(mightNeedles[1]) !== -1){

    for(t=0; t<mightNeedles.length; t++){

      if(fontName.indexOf(mightNeedles[t]) !== -1){

        fontName = fontName.replace(mightNeedles[t], " ");

        return styleName(fontName);
      }
    }


  }else{

    return styleName(fontName);
  }
}

function writeNewFontTable(){

  var fonts = document.querySelectorAll("[data-form='fonts'] .subfield input[name='filenames[]']");
  var dir = document.getElementById("write-font").getAttribute("data-path");

  for(i=0; i<fonts.length; i++){

    var path = dir+"/"+fonts[i].value; 

    (function(i){       

      opentype.load(path, function(err, font){

        var fontMetrics = font.tables.os2;
        var fontNamingTable = font.tables.name;
        var fontHeaderTable = font.tables.head;
        var glyphs = font.glyphs.glyphs;
        var gCount = Object.keys(glyphs).length;

        if(document.body.classList.contains("check-font")){

          if(i == 0){

            var metasGeneral = {

              license: (typeof fontNamingTable.license !== 'undefined') ? fontNamingTable.license.en : "",
              licenseUrl: (typeof fontNamingTable.license !== 'undefined') ? fontNamingTable.license.en : "",
              glyphCount: gCount,
              fontAscender: fontMetrics.sTypoAscender,
              fontDescender: fontMetrics.sTypoDescender,
              fontXHeight: fontMetrics.sxHeight   

            };

            setValues(metasGeneral, 1);
          }

          var subfam1 = (typeof fontNamingTable.fontSubfamily !== 'undefined') ? fontNamingTable.fontSubfamily.en : "";
          var subfam2 = (typeof fontNamingTable.preferredSubfamily !== 'undefined') ? fontNamingTable.preferredSubfamily.en : "";
          var styles;



          if(subfam1 !== ""){

            styles = (subfam1 !== subfam2) ? subfam1+" "+subfam2 : subfam1;

          }else{

            styles = setStyles(font);

          }

          var metasSpecific = {

            designerName: (typeof fontNamingTable.designer !== 'undefined')? fontNamingTable.designer.en : "",
            designerUrl: (typeof fontNamingTable.designerURL !== 'undefined') ? fontNamingTable.designerURL.en : "",
            styles: styles

          }

          setValues(metasSpecific, 0, i);

        }

        var charsets = fonts[i].parentElement.querySelectorAll(".charset");
        var length = 0;

        charsets.forEach(function(charset){

          for (glyph in glyphs){

            length ++;

            if(glyphs[glyph].unicode){

              if(glyph < 98 && glyphs[glyph].name !== "space" && glyphs[glyph].name !== "nonmarkingreturn"){

                charset.innerHTML +="<span>&#"+glyphs[glyph].unicode+";</span>";

              }

            }
          }

        });

      });

    })(i);


  }
}

function setTag(tagField, sign1, sign2){

  var text = tagField.value;
  let needle1 = sign2;
  let alreadyTags = text.lastIndexOf(needle1);

  if(alreadyTags !== -1){

    let where = alreadyTags+needle1.length;
    let tag = text.substring(where);
    let newText = text.substring(0, text.lastIndexOf(tag));

    if(tag.length > 0){

      tagField.value=newText+sign1+tag+sign2;
    }

  }else{

    let tag = text;

    if(tag.length > 0){

      tagField.value=sign1+tag+sign2;

    }
  }

}

function preventValidateOnEnter(){

  document.addEventListener("keypress", function(e){

    var forms = document.querySelectorAll("form");

    forms.forEach(function(form){

      if(enterPressed(e)){

        form.setAttribute("novalidate", true);
        form.addEventListener("submit", function(k){

          k.preventDefault();

        });

      }
    });
  });
}

function clickValiadtion(e){

  return (e.clientX > 0 && e.clientY > 0) ? true : false;
}

function allDifferentStyles(form){

  var styles = document.querySelectorAll("input[name='styles[]']");

  if(styles){

   var values = [];

   styles.forEach(function(style, index){

    values.push(style.value);

  });


   for(h=0; h<styles.length; h++){

    var checks = values.slice(h+1, values.length);
    var hasDouble = (checks.indexOf(styles[h].value) !== -1) ? true : false;

    if(hasDouble){

      var int = h+1+checks.indexOf(styles[h].value);

      styles[h].classList.add("wrong");
      styles[int].classList.add("wrong");

      alert("All style values must be different.");

      break;
      return false;


    }

    if(h == styles.length-1){

      return true;
    }


  }
}
}


function formValidation(){

  preventValidateOnEnter();

  var submits = document.querySelectorAll("input[type='submit']");

  submits.forEach(function(submit){

    submit.addEventListener("click", function(p){

      if(clickValiadtion(p)){

        var form = submit.parentElement.parentElement;

        if(allDifferentStyles(form)){

          form.removeAttribute("novalidate");
          form.submit();

        }else{

          p.preventDefault();
        }
      }

    });

  });
}

function makeTags(){

  let tagFields = document.querySelectorAll("input[data-field-type='tags']");

  tagFields.forEach(function(tagField){

    tagField.addEventListener("keypress", function(e){

      if(enterPressed(e)){

        setTag(tagField, "[ ", " ]");

      }

    });

  });
}

function setRemoveEntry(button, div){

  button.addEventListener("click", function(){

    var par = div.parentElement;
    par.removeChild(div);

  });

}

function emptyClone(clone, num){

  clone.dataset.num = num+1;

  var inputs = clone.querySelectorAll("input");

  inputs.forEach(function(input){

    var needle = input.id.indexOf("-");
    var newNum = parseInt(input.id.substr(needle+1))+1;
    var name = input.id.substr(0, needle+1);

    input.value="";
    input.id= name+newNum;

  });

  var button = document.createElement("button");
  button.classList.add("remove");
  button.innerHTML="Remove";

  setRemoveEntry(button, clone);

  clone.appendChild(button);

}

function setRemoveEntries(){

  var removeButts = document.querySelectorAll(".remove");

  removeButts.forEach(function(removeButt){

    var par2 = removeButt.parentElement.parentElement;

    setRemoveEntry(removeButt, par2);

  });
}

function addEntry(){

  var addButt = document.getElementById("addButt");

  addButt.addEventListener("click", function(){

    var formName = this.dataset.form;
    var container = document.querySelector(".subfields[data-form='"+formName+"']");
    var formDiv = container.querySelector(".subfield");
    var num = parseInt(formDiv.dataset.num);
    var clone = formDiv.cloneNode(true);

    emptyClone(clone, num);

    container.appendChild(clone);

  });
}


/* ---- MODIFY-FONT ---- */

function areUSure(){

  var form = document.getElementById("delete");

  form.addEventListener("submit", function(e){

    var sure = confirm("Are you sure you want to delete this font ?");

    if(!sure){

      e.preventDefault();

    }

  });
}

/* ---- ACCOUNT ---- */

function checkPass(){

  var form = document.getElementsByTagName("FORM")[0];

  form.addEventListener("submit", function(e){

    var pass1 = document.getElementById("pass").value;
    var pass2 = document.getElementById("pass_confirm").value;
    var isSame = (pass1 == pass2) ? true : false;

    if(!isSame){

      e.preventDefault();
      alert("Passwords must be the same.");
    }


  });

}


/* ---- LAUNCH ---- */

function isPage(page){

  return (document.body.classList.contains(page)) ? true : false;

}

function setScripts(){

  document.addEventListener("DOMContentLoaded", function(){

    if(isPage("modify-font") || isPage("check-font")){

        writeNewFontTable();
        makeTags();
        formValidation();
        addEntry();
        setRemoveEntries();

      if(isPage("modify-font")){

        areUSure();

      }

    }else if(isPage("new-account")){

      checkPass();

    }

  });

}

setScripts();

