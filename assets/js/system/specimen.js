const dir = '../content/fonts'
const setchars = document.getElementsByClassName('setchars')	
const cvsProbe = document.getElementsByClassName('probe')	
const info = document.getElementsByClassName('info')	
const block = document.getElementsByClassName('block')	
const collections = document.querySelectorAll('.collection')	
const menu = document.querySelectorAll('#menu .btn-font')
const contsvg = document.querySelectorAll('.probesvg')

var family = []
var familyFile = []

for (var i = 0; i < menu.length; i++) {
	family.push(menu[i].getAttribute('data-fontname'))
	familyFile.push(menu[i].getAttribute('data-file'))
}

var kerning = true
var ligatures = true
var hinting = true

function mapChars(font){
	var all = []
	var glyphs = font.glyphs.glyphs
	
	for (var item in glyphs) {

		var glyph = glyphs[item]
		if (glyph.unicode != null) {
			all += '<div class="char"	data-char="&#' + glyph.unicode + ';" data-title="' + glyph.name + ' | ' + glyph.unicode + '"><span>&#' + glyph.unicode + ';</span></div>'
			// all += glyph.unicode;
		}
	}	
	setchars[0].innerHTML = all	
}


function renderTxt(txt, mode, font, fntsize, ctx) {

	var options = {
		kerning: kerning,
		hinting: hinting,
		features: {
				liga: ligatures,
				rlig: ligatures
	}}

	var path = font.getPath(txt, 75, fntsize, fntsize, options)

	if (mode == 'fill') {
		path.fill = 'black'
	} else if (mode == 'points') {
		path.fill = 'white'
		path.stroke = 'red'
		font.drawPoints(ctx, txt, 75, fntsize, fntsize, options)
		// font.drawMetrics(ctx, txt, 75, fntsize, fntsize, options)
	}

	path.draw(ctx)

}
function drawHints(font, txt, ctx, w, y, fntsize) {	
	var scale =  font.unitsPerEm / fntsize
	ctx.beginPath()
	ctx.moveTo(70, fntsize - y / scale)
	ctx.lineTo(w, fntsize - y / scale)
	ctx.strokeStyle = "grey"
	ctx.stroke()
	ctx.font = '12px notCourierReg'
	ctx.fillStyle = 'red'
	ctx.fill()
	ctx.fillText(txt, 10, fntsize - (y / scale) + 3)
}

function listHints(font, ctx, width, fntsize) {
	drawHints(font, 'Baseline', ctx,  width, 0, fntsize)
	drawHints(font, 'Ascender', ctx, width, font.tables.os2.sTypoAscender, fntsize)
	drawHints(font, 'Cap Height', ctx, width, font.tables.os2.sCapHeight, fntsize)
	drawHints(font, 'X Height', ctx, width, font.tables.os2.sxHeight, fntsize)
	drawHints(font, 'Descender', ctx, width, font.tables.os2.sTypoDescender, fntsize)
}

// function interProbe(font, text, ctx, width, height, fntsize){	
// 	ctx.clearRect(0, 0, width, height)
// 	if (BtnBlack.getAttribute('data-points') == 'true' ) {
// 		renderTxt(text, 'points', font, fntsize, ctx)
// 		listHints(font, ctx, width, fntsize)
// 	} else {
// 		renderTxt(text, 'fill', font, fntsize, ctx)
// 	}	
// }
//

// function probeGlyphs(i, font){
// 	let height = font.ascender + font.descender
//
// 	var fntsize = cvsSvg[i]).fontSize)
// 	var cvsheight = fntsize + (fntsize / 3) 
// 	var cvswidth = cvsProbe[i].offsetWidth 	
// 	var cvstext = cvsProbe[i].innerText 	
// 	var canvas = document.createElement('canvas')
// 			canvas.id = "probe_canvas" + i
// 			canvas.height = cvsheight
// 			canvas.width = cvswidth
// 	var inputTxt = document.createElement('input')	
// 			inputTxt.id = 'inputTxt' + i
// 			inputTxt.setAttribute("type", "text")
// 			inputTxt.setAttribute("value", cvstext)
// 	var BtnBlack = document.createElement('span')	
// 			BtnBlack.id = 'BtnBlack'
// 			BtnBlack.setAttribute("data-points", 'true')
// 			BtnBlack.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12" version="1.1"><circle cx="6" cy="6" r="6"/></svg>'
//
// 	cvsProbe[i].style.fontSize = 0
// 	cvsProbe[i].innerHTML = ""
// 	cvsProbe[i].appendChild(inputTxt)
// 	cvsProbe[i].appendChild(BtnBlack)
// 	cvsProbe[i].appendChild(canvas)
// 	
// 	var ctx = cvsProbe[i].querySelector("#probe_canvas" + i).getContext('2d')
//
// 	inputTxt.addEventListener('input', function() {
// 		var text = this.value
// 		interProbe(font, text, ctx, cvswidth, cvsheight, fntsize);
// 	}) 
//
// 	BtnBlack.addEventListener('click', function() {
// 		var text = inputTxt.value
// 		if (this.getAttribute('data-points') == 'true' ) {
// 			this.setAttribute('data-points', 'false')					
// 			interProbe(font, text, ctx, cvswidth, cvsheight, fntsize);
// 		} else {
// 			this.setAttribute('data-points', 'true')
// 			interProbe(font, text, ctx, cvswidth, cvsheight, fntsize);
// 			listHints(font, ctx, cvswidth, fntsize)
// 		}
// 	}) 	
//
// 	if (BtnBlack.getAttribute('data-points') == 'true' ) {
// 		renderTxt(cvstext, 'points', font, fntsize, ctx)
// 		listHints(font, ctx, cvswidth, fntsize)
// 	} else {
// 		renderTxt(cvstext, 'fill', font, fntsize, ctx)
// 	}
// 	window.onresize = function(event) {
// 		if (BtnBlack.getAttribute('data-points') == 'true' ) {
// 			renderTxt(cvstext, 'points', font, fntsize, ctx)
// 			listHints(font, ctx, cvswidth, fntsize)
// 		} else {
// 			renderTxt(cvstext, 'fill', font, fntsize, ctx)
// 		}	
// 	}
// 	
// }

function headInfo(objt, i) {
	var cObjt = window.getComputedStyle(objt[i])	
	var fntFamily = cObjt.fontFamily
	var fntSize = cObjt.fontSize
	var fntsize = parseInt(cObjt.fontSize)
	var message = fntsize + ' - ' + fntFamily
	var header = document.createElement('div')  
			header.id = 'headInfo'
			header.setAttribute('class', 'headInfo')
			header.setAttribute("contenteditable", "false")
			header.innerHTML = message 

	var elem = objt[i].querySelector('#headInfo')

	if (elem) {
		elem.innerHTML = message 	
	} else {
		objt[i].insertAdjacentElement('afterbegin', header)	
	}

}

function specimen(font) {

	if (setchars.length > 0) {
		mapChars(font)
	}

	if (info.length > 0) {
		for (var i = 0; i < info.length; i++) {
			headInfo(info, i)
		}
	}
	probeSVG(0, font);

	window.onresize = function(event) {
		for (var i = 0; i < info.length; i++) {
			headInfo(info, i)
		}
		for (var i = 0; i < info.length; i++) {
			headInfo(block, i)
		}
	}	

}

function dPoint (x, y, color, event) {
		var svgns = "http://www.w3.org/2000/svg";
		var shape = document.createElementNS(svgns, "circle");
				shape.setAttributeNS(null, "cx", x)
				shape.setAttributeNS(null, "cy", y)
				shape.setAttributeNS(null, "r", 4)
				shape.setAttributeNS(null, "stroke-width", 1)
				shape.setAttributeNS(null, "stroke",  'white')
				shape.setAttributeNS(null, "fill", color)
		return shape.outerHTML
}

function dHints (x, y, w,  color, text) {
		var svgns = "http://www.w3.org/2000/svg"
		var hint = document.createElementNS(svgns, "path")
				hint.setAttributeNS(null, "d", 'M ' + x + ',' + y + ' H ' + w)
				hint.setAttributeNS(null, "stroke-width", 1)
				hint.setAttributeNS(null, "class",  "hints")

		var title = document.createElementNS(svgns, "text")
				title.innerHTML = text
				title.setAttributeNS(null, "y",  y + 4)
				title.setAttributeNS(null, "class",  "hints-title")

		return hint.outerHTML + title.outerHTML
}

function writeSvg(font, gInfo, i) {

	var prtSvg = document.getElementById('probesvg' + i)
			// prtSvg.innerHTML = ""
	var text = document.querySelectorAll('#inputTxt' + i)[0].value
		
	tP = font.getPath(text, 80, gInfo.fontSize, gInfo.fontSize)
		
	var compose = []

	for( z of tP.commands) {	
		if (z.x !== undefined) {
			compose += dPoint(z.x, z.y, 'blue')
		}
		if (z.x1 !== undefined) {
			compose += dPoint(z.x1, z.y1, 'red')
		}
		if (z.x2 !== undefined) {
			compose += dPoint(z.x2, z.y2, 'red')
		}
	}

	compose += dHints(80, gInfo.fontSize, gInfo.width, 'purple', 'baseline') 	

	if (font.tables.os2.sxHeight !== undefined) {
		compose += dHints(80, gInfo.fontSize - font.tables.os2.sxHeight / gInfo.scale, gInfo.width, 'purple', 'xHeight') 	
	}
	if (font.tables.os2.sCapHeight !== undefined) {
		compose += dHints(80, gInfo.fontSize - font.tables.os2.sCapHeight / gInfo.scale, gInfo.width, 'purple', 'CapHeight') 	
	}
	if (font.tables.os2.sTypoDescender !== undefined) {
		compose += dHints(80, gInfo.fontSize - font.tables.os2.sTypoDescender / gInfo.scale, gInfo.width, 'purple', 'Descender') 	
	}
	if (font.tables.os2.sTypoAscender !== undefined) {
		compose += dHints(80, gInfo.fontSize - font.tables.os2.sTypoAscender / gInfo.scale, gInfo.width, 'purple', 'Ascender')	
	}
	
	var pathGlyphs = '<path d="' + tP.toPathData() +'" class="pathGlyphs" />'

	console.log(window.event.keyCode)
	prtSvg.innerHTML = pathGlyphs + compose

}

function getValues(i, font) {
	var size = contsvg[i].getAttribute('data-size')
	
	var g = {
			height: font.ascender + font.descender,
			width: contsvg[i].offsetWidth,
			fontSize: 400,
			text: contsvg[i].innerText,
			scale: font.unitsPerEm / 400 
	}

	return g
}

function probeSVG(i, font){
	
	contsvg[i].setAttribute('data-size', parseInt(window.getComputedStyle(contsvg[i]).fontSize))
	
	gInfo = getValues(i, font)
	// SVG 	
	var svg = document.createElement('svg')
			svg.id = 'probesvg' + i
			svg.setAttribute('width', '100%')
			svg.setAttribute('height', '100%')
			svg.setAttribute('viewbox', '0 100 ' + gInfo.width + ' ' + gInfo.fontSize)

	// Input text	
	var inputTxt = document.createElement('input') 
			inputTxt.id = 'inputTxt' + i
			inputTxt.setAttribute("type", "text")
			inputTxt.setAttribute("value", gInfo.text)

	contsvg[i].setAttribute('style', 'fill: #D0D0D0;  height:' + (gInfo.fontSize + (gInfo.fontSize / 3)) + 'px; width:' + gInfo.width + 'px')
	contsvg[i].innerHTML = inputTxt.outerHTML + svg.outerHTML
	
	writeSvg(font, gInfo, i)
	inp = document.getElementById('inputTxt' + 0)
	inp.focus()
	inp.addEventListener('input', function() {
		gInfo.text = this.value
		writeSvg(font, gInfo, 0)
	}) 


}



function loadSpecimen(url, reload) {

	opentype.load(url, function(err, font) {
		if(err) {
				alert('Could not load font: ' + err)
		} else if (reload == true) {
			var gInfo = getValues(0, font)
			writeSvg(font, gInfo, 0)

			inp = document.getElementById('inputTxt' + 0)
			inp.addEventListener('input', function() {
				gInfo.text = this.value
				writeSvg(font, gInfo, 0)
			}) 
			// interProbe(font, text, ctx,  w, h, fntsize)
		} else {
			specimen(font)
		}
	})
}

function changeFont(fontName, url, select) {

	for(let elem of menu) {
		elem.setAttribute('select', 'false')
	}
	select.setAttribute('select', 'true')
	document.getElementById('specimen-pages').style.fontFamily = fontName
	loadSpecimen(url, true)
}

function collectionBuild(elems) {
	var i = 0	
	for (var elem of elems) {
		// var stl = elem
		var stl = elem.style.cssText
		elem.id = 'orrigine' + i
		elem.classList.remove('collection')
		elem.style.cssText = '' 
		elem.style.fontFamily = family[0]
		var wr = document.createElement('div')
				wr.style.cssText = stl
				wr.id = 'wrapper_collection_' + i
				wr.className = 'wrapper_collection'
		var prt = elem.parentNode
				prt.replaceChild(wr, elem);
		wr.appendChild(elem)
		for (var u = 1; u < family.length; u++) {
			var n = elem.cloneNode(true)
			n.removeAttribute('id')
			n.style.fontFamily = family[u]
			wr.appendChild(n)
		}	
		i++	
	}
}


window.addEventListener('DOMContentLoaded', function(){

	var wrapper = document.getElementById("specimen-pages")
	var name = document.querySelectorAll('#menu .btn-font[select="true"]')[0].getAttribute('data-file')
	var path = dir + '/' + wrapper.dataset.path
	var font = path + '/' + name 

	loadSpecimen(font, false)

	for(let elem of menu) {
    	elem.addEventListener("click", e => changeFont(elem.getAttribute('data-fontname'), path + '/' + elem.getAttribute('data-file'), elem), true)
  }

	if (collections.length > 0) {
		collectionBuild(collections)	
	}


})
