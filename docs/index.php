<?php include("snippets/header.php") ?>
<main>
	<?php foreach($docs->getParts() as $part): ?>
		<section id="<?= $part->uid ?>">
			<h2><?= $part->title ?></h2>
			<?= $part->content ?>
			<?php if(isset($part->subpages)): ?>
				<?php foreach($part->subpages as $subpage): ?>
					<h3><?= $subpage->title ?></h3>
					<?= $subpage->content ?>
				<?php endforeach ?>
			<?php endif ?>	
		</section>
	<?php endforeach ?>	
</main>
<?php include("snippets/footer.php") ?>