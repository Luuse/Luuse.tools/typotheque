<nav>
	<?php foreach($docs->getParts() as $part): ?>
		<a class="page" href="#<?= $part->uid ?>"><?= $part->title ?></a>
		<?php if(isset($part->subpages)): ?>
			<?php foreach($part->subpages as $subpage): ?>
				<a class="subpage" href="#<?= $subpage->uid ?>"><?= $subpage->title ?></a>
			<?php endforeach ?>	
		<?php endif ?>	
	<?php endforeach ?>	
</nav>