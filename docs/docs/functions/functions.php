<?php

require("docs/lib/parsedown-master/Parsedown.php");


class Docs{

	/* STRINGS */

	public function getLastFilename($path){

		$start = strrpos($path, "/")+1;
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		$length = strlen($path) - ($start + strlen($ext));

		return substr($path, $start, $length);

	}


	public function firstCap($str){

		return strtoupper(substr($str, 0, 1)).substr($str, 1);
	}

	public function stripNumber($folderName){

		$start = strpos($folderName, "-")+1;

		return substr($folderName, $start);

	}

	public function getUid($folderName){

		return $this->stripNumber($this->getLastFilename($folderName));
	}


	public function toTitle($uid){


		if(is_int(strpos($uid, "-"))){

			$splited = explode("-", $uid);
			$joined = [];

			foreach($splited as $part){

				$joined [] = $this->firstCap($part);
			}

			return implode(" ", $joined);

		}else{

			return $this->firstCap($uid);
		}
	}


	/* MARKDOWN */

	public function parsedown($txt){

		$parsedown = new Parsedown();

		return $parsedown->text($txt);
	}

	/* CONTENT */

	public function getFilename($dir){

		return $dir."/".$this->getUid($this->getLastFilename($dir)).".md";
	}

	public function fillPageInfos($dir, $content, $file){

		$name = $this->getLastFilename($dir);
		$uid = $this->getUid($name);

		$content->file = $file;
		$content->uid = $uid;
		$content->title = $this->toTitle($uid);
		$content->content = $this->parsedown(file_get_contents($file));

		return $content;

	}

	public function getParts(){

		$dirs = glob("content/*");
		$files = [];

		foreach ($dirs as $dir) {

			if(is_dir($dir)){

				$content = new stdClass();
				$subdirs = glob($dir."/*");

				if(count($subdirs) > 0){

					foreach($subdirs as $subdir){

						if(is_dir($subdir)){

							$subfile= $this->getFilename($subdir);

							if(file_exists($subfile)){

								$subcontent = new stdClass();
								$content->subpages[] = $this->fillPageInfos($subdir, $subcontent, $subfile);

							}
						}
					}
				}

				$file = $this->getFilename($dir);

				if(file_exists($file)){

					$files [] = $this->fillPageInfos($dir, $content, $file);
				}

			}
		}

		return $files;

	}

}


$docs = new Docs;


?>
